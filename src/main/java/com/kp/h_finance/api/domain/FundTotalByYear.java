package com.kp.h_finance.api.domain;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Map;

@Data
public class FundTotalByYear {
    private String year;
    private long total_amount;
    private Map<String, Long> detail_amount;
}
