package com.kp.h_finance.controller;

import com.kp.h_finance.api.service.CSVFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainController {

    @Autowired
    private CSVFileService csvFileService;

    @GetMapping("")
    public String root() {
        return "redirect:/main";
    }

    @GetMapping("/main")
    public ModelAndView main() {
        return new ModelAndView("/main");
    }

    @PostMapping("/upload")
    public String upload(
            @RequestParam("uploadFile") MultipartFile uploadFile
    ) throws Exception{

        csvFileService.upload(uploadFile);

        return "redirect:/main";
    }

}
