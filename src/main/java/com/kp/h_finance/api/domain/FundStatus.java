package com.kp.h_finance.api.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class FundStatus {

    @Id
    @GeneratedValue
    private long id;

    private int year;
    private int month;

    @OneToOne(targetEntity = Institute.class)
    @JoinColumn(name="INSTITUTE_CODE")
    private Institute institute;

    private int credit_guarantee_amount;
}
