package com.kp.h_finance.api.service;

import au.com.bytecode.opencsv.CSVReader;
import com.kp.h_finance.api.domain.FundStatus;
import com.kp.h_finance.api.domain.Institute;
import com.kp.h_finance.api.repository.FundStatusRepository;
import com.kp.h_finance.api.repository.InstituteRepository;
import com.kp.h_finance.common.util.FileUtil;
import com.kp.h_finance.common.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service("csvFileService")
public class CSVFileService {

    @Autowired
    private FundStatusRepository fundStatusRepository;

    @Autowired
    private InstituteRepository instituteRepository;

    @Transactional
    public void upload(MultipartFile file) {
        try {
            String charset = FileUtil.getFileCharset(FileUtil.multipartToFile(file));
            CSVReader reader = new CSVReader(new InputStreamReader(file.getInputStream(), charset), ',', '"', 0);
            String[] sArr;
            Map<Integer, Institute> instituteMap = new HashMap<>();

            if (reader != null) {
                fundStatusRepository.deleteAll();

                while ((sArr = reader.readNext()) != null) {
                    for (int i = 0; i < sArr.length; i++) {
                        if (!StringUtil.isNumber(sArr[0])) {
                            // title 처리 : institute 등록
                            if (i > 1) {
                                Institute institute = new Institute();
                                institute.setName(StringUtil.getInstituteName(sArr[i]));
                                instituteRepository.save(institute);
                                instituteMap.put(i, institute);
                            }
                        } else {
                            FundStatus fundStatus = new FundStatus();
                            fundStatus.setYear(Integer.parseInt(sArr[0]));
                            fundStatus.setMonth(Integer.parseInt(sArr[1]));
                            fundStatus.setInstitute(instituteMap.get(i));
                            fundStatus.setCredit_guarantee_amount(Integer.parseInt(sArr[i].replaceAll(",", "")));

                            fundStatusRepository.save(fundStatus);
                        }
                    }
                }
            }
        } catch (FileNotFoundException e) {
            log.error(e.getMessage());
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }
}
