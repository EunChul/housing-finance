package com.kp.h_finance.common.comparator;

import com.kp.h_finance.api.domain.FundTotalByYear;

import java.util.Comparator;

public class HousingFinanceComparatorByYear implements Comparator<FundTotalByYear> {
    @Override
    public int compare(FundTotalByYear hf1, FundTotalByYear hf2) {
        return hf1.getYear().compareTo(hf2.getYear());
    }
}
