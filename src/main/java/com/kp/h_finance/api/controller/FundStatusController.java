package com.kp.h_finance.api.controller;

import com.kp.h_finance.api.domain.FundTotalByBank;
import com.kp.h_finance.api.domain.FundTotalByYear;
import com.kp.h_finance.api.domain.Institute;
import com.kp.h_finance.api.service.FundStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class FundStatusController {

    @Autowired
    private FundStatusService fundStatusService;

    @GetMapping("/institutes")
    public List<Institute> getInstitute() {
        return fundStatusService.getAllInstitute();
    }

    @GetMapping("/fund-status/total-amount")
    public List<FundTotalByYear> getFundStatusTotal() {
        return fundStatusService.getFundStatusTotal();
    }

    @GetMapping("/fund-status/biggest-amount-bank")
    public Map<String,String> getBiggestAmountBank() {
        return fundStatusService.getBiggestAmountBank();
    }

    @GetMapping("/fund-status/외환은행/bigAndSmall")
    public Map<String, Object> getKoreaExchangeBankBigAndSmallFund() {
        return fundStatusService.getKoreaExchangeBankBigAndSmallFund();
    }
}
