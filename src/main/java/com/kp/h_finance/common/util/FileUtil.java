package com.kp.h_finance.common.util;

import lombok.extern.slf4j.Slf4j;
import org.mozilla.universalchardet.UniversalDetector;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

@Slf4j
public class FileUtil {

    public static String getFileCharset(File file){
        byte[] buf = new byte[4096];
        UniversalDetector detector = new UniversalDetector(null);
        int nread;

        try {
            FileInputStream fis = new FileInputStream(file);
            while ((nread = fis.read(buf)) > 0 && !detector.isDone()) {
                detector.handleData(buf, 0, nread);
            }
        }catch (IOException e){
            log.error(e.getMessage());
        }
        detector.dataEnd();
        String encoding = detector.getDetectedCharset();

        detector.reset();

        return encoding;
    }

    public static File multipartToFile(MultipartFile multipart){
        File convFile = new File(multipart.getOriginalFilename());
        try {
            convFile.createNewFile();

            try (FileOutputStream fos = new FileOutputStream(convFile)) {
                fos.write(multipart.getBytes());
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }catch (IOException e){
            log.error(e.getMessage());
        }
        return convFile;
    }
}
