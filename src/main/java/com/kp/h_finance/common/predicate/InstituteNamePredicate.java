package com.kp.h_finance.common.predicate;

import java.util.function.Predicate;
import java.util.regex.Pattern;

public class InstituteNamePredicate implements Predicate<String> {
    private final Pattern pattern  =  Pattern.compile("[ㄱ-ㅎ가-힣||\\/]");

    @Override
    public boolean test(String s) {
        return pattern.matcher(s).find();
    }
}
