package com.kp.h_finance.api.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.kp.h_finance.api.domain.FundTotalByBank;
import com.kp.h_finance.api.domain.FundTotalByYear;
import com.kp.h_finance.api.domain.Institute;
import com.kp.h_finance.api.repository.FundStatusRepository;
import com.kp.h_finance.api.repository.InstituteRepository;
import com.kp.h_finance.common.comparator.HousingFinanceComparatorByYear;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigInteger;
import java.util.*;

@Service
public class FundStatusService {

    @Autowired
    private FundStatusRepository fundStatusRepository;

    @Autowired
    private InstituteRepository instituteRepository;

    public List<Institute> getAllInstitute() {
        return instituteRepository.findAll();
    }

    public List<FundTotalByYear> getFundStatusTotal() {
        List<FundTotalByYear> fundTotalByYearList = Lists.newArrayList();

        List<FundTotalByBank> bankFundTotalByBanks = findTotalAmountGroupByYear();

        if (!CollectionUtils.isEmpty(bankFundTotalByBanks)) {
            Map<Integer, FundTotalByYear> yearTotalAmount = new HashMap<>();
            for (FundTotalByBank bankFundTotalByBank : bankFundTotalByBanks) {
                if (yearTotalAmount.containsKey(bankFundTotalByBank.getYear())) {
                    FundTotalByYear fundTotalByYear = yearTotalAmount.get(bankFundTotalByBank.getYear());
                    fundTotalByYear.setTotal_amount(fundTotalByYear.getTotal_amount() + bankFundTotalByBank.getTotal_amount());
                    fundTotalByYear.getDetail_amount().put(bankFundTotalByBank.getInstitute_name(), bankFundTotalByBank.getTotal_amount());
                } else {
                    FundTotalByYear fundTotalByYear = new FundTotalByYear();
                    fundTotalByYear.setYear(bankFundTotalByBank.getYear() + "년");
                    fundTotalByYear.setTotal_amount(bankFundTotalByBank.getTotal_amount());
                    Map<String, Long> detailAmountMap = new HashMap<>();
                    detailAmountMap.put(bankFundTotalByBank.getInstitute_name(), bankFundTotalByBank.getTotal_amount());
                    fundTotalByYear.setDetail_amount(detailAmountMap);
                    yearTotalAmount.put(bankFundTotalByBank.getYear(), fundTotalByYear);
                }
            }

            fundTotalByYearList = new ArrayList<>(yearTotalAmount.values());
        }

        Collections.sort(fundTotalByYearList, new HousingFinanceComparatorByYear());

        return fundTotalByYearList;
    }

    public Map<String, String> getBiggestAmountBank() {
        Map<String, String> result = new LinkedHashMap<>();
        List<FundTotalByBank> bankFundTotalByBanks = findTotalAmountGroupByYear();
        if (!CollectionUtils.isEmpty(bankFundTotalByBanks)) {
            Collections.sort(bankFundTotalByBanks, new Comparator<FundTotalByBank>() {
                @Override
                public int compare(FundTotalByBank o1, FundTotalByBank o2) {
                    return (int) (o2.getTotal_amount() - o1.getTotal_amount());
                }
            });

            result.put("year", String.valueOf(bankFundTotalByBanks.get(0).getYear()));
            result.put("bank", String.valueOf(bankFundTotalByBanks.get(0).getInstitute_name()));
        }

        return result;
    }

    public Map<String, Object> getKoreaExchangeBankBigAndSmallFund() {
        Institute koreaExchangeBank = instituteRepository.findByName("외환은행");
        List<FundTotalByBank> fundTotalByBanks = findTotalAmountGroupByYear(koreaExchangeBank.getName());
        Collections.sort(fundTotalByBanks, new Comparator<FundTotalByBank>() {
            @Override
            public int compare(FundTotalByBank o1, FundTotalByBank o2) {
                return (int)(o1.getTotal_amount() - o2.getTotal_amount());
            }
        });

        Map<String, Object> resultMap = new HashMap<>();
        if(!CollectionUtils.isEmpty(fundTotalByBanks)) {
            List<FundTotalByBank> bigAndSmall = Lists.newArrayList();
            bigAndSmall.add(fundTotalByBanks.get(0));
            bigAndSmall.add(fundTotalByBanks.get(fundTotalByBanks.size() -1));

            resultMap.put("bank", koreaExchangeBank.getName());
            resultMap.put("support_amount", bigAndSmall);
        }
        return resultMap;
    }

    private List<FundTotalByBank> findTotalAmountGroupByYear() {
        return findTotalAmountGroupByYear(null);
    }

    private List<FundTotalByBank> findTotalAmountGroupByYear(String instituteName) {
        List<Object[]> list = fundStatusRepository.findTotalAmountGroupByYear();
        List<FundTotalByBank> fundTotalByBanks = Lists.newArrayList();
        for (Object[] o : list) {
            if(!StringUtils.isEmpty(instituteName)){
                if(!instituteName.equals(o[1])){
                    continue;
                }
            }
            FundTotalByBank fundTotalByBank = new FundTotalByBank();
            fundTotalByBank.setYear((Integer) o[0]);
            fundTotalByBank.setInstitute_name((String) o[1]);
            fundTotalByBank.setTotal_amount(((BigInteger) o[2]).longValue());

            fundTotalByBanks.add(fundTotalByBank);
        }
        return fundTotalByBanks;
    }
}
