package com.kp.h_finance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HFinanceApplication {

	public static void main(String[] args) {
		SpringApplication.run(HFinanceApplication.class, args);
	}

}
