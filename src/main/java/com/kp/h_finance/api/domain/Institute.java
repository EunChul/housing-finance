package com.kp.h_finance.api.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Institute {

    @Id
    @GeneratedValue
    @Column(name = "INSTITUTE_CODE")
    private long code;

    private String name;
}
