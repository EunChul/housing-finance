package com.kp.h_finance.common.predicate;

import java.util.function.Predicate;
import java.util.regex.Pattern;

public class NumberCheckPredicate implements Predicate<String> {
    private final Pattern pattern  =  Pattern.compile("^[0-9]+$");

    @Override
    public boolean test(String s) {
        return pattern.matcher(s).find();
    }
}
