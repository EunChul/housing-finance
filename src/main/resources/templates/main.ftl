<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.4.1.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <title>주택 금융 서비스</title>

    <script>
        $(document).ready(function () {
            $('#uploadBtn').click(function () {
                var frm = $('#uploadForm');
                frm.submit();
            });

            $('#findBankList').click(function() {
                $.ajax({
                    url : "/institutes",
                    dataType : "text",
                    success : function(data){
                        if(data != null && data.length > 0){
                            $('#result').val(data);
                        }else{
                            $('#result').html("데이터가 없습니다.");
                        }
                    }
                });
            });
            $('#findTotalAmount').click(function() {
                $.ajax({
                    url : "/fund-status/total-amount",
                    dataType : "text",
                    success : function(data){
                        if(data != null && data.length > 0){
                            $('#result').val(data);
                        }else{
                            $('#result').html("데이터가 없습니다.");
                        }
                    }
                });
            });
            $('#findBiggestAmountBank').click(function() {
                $.ajax({
                    url : "/fund-status/biggest-amount-bank",
                    dataType : "text",
                    success : function(data){
                        if(data != null && data.length > 0){
                            $('#result').val(data);
                        }else{
                            $('#result').html("데이터가 없습니다.");
                        }
                    }
                });
            });
            $('#findAvgBigAndSmall').click(function() {
                $.ajax({
                    url : "/fund-status/외환은행/bigAndSmall",
                    dataType : "text",
                    success : function(data){
                        if(data != null && data.length > 0){
                            $('#result').val(data);
                        }else{
                            $('#result').html("데이터가 없습니다.");
                        }
                    }
                });
            });
        });
    </script>
</head>
<body>
<div>
    <h1>주택금융 공급현황</h1>
    <hr/>
    <div class="col-md-6">
        <form id="uploadForm" class="form-inline" action="upload" method="post" enctype="multipart/form-data">
            <div class="col-md-5">
                <label for="uploadFile">초기 데이타 등록 : </label>
            </div>
            <div class="form-group col-md-5">
                <input type="file" class="form-control-file" id="uploadFile" name="uploadFile"/>
            </div>
            <button type="button" class="btn btn-primary col-md-2" id="uploadBtn">Upload</button>
        </form>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-primary" id="findBankList">은행목록조회</button>
            <button type="button" class="btn btn-success" id="findTotalAmount">년도별 지원금액 합계조회</button>
            <button type="button" class="btn btn-warning" id="findBiggestAmountBank">최대 지원금액 기관 조회</button>
            <button type="button" class="btn btn-danger" id="findAvgBigAndSmall">외환은행 년 평균 최대 최소 금액 조회</button>
        </div>
    </div>

    <div class="form-group" style="margin: 50px 0 0 50px;">
        <textarea id="result" class="form-control col-md-6" style="height: 200px;overflow: scroll"></textarea>
    </div>
</div>
</body>
</html>