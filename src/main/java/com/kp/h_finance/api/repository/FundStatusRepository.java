package com.kp.h_finance.api.repository;

import com.kp.h_finance.api.domain.FundStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FundStatusRepository extends JpaRepository<FundStatus, Long> {

    /*@Query("SELECT new com.kp.h_finance.api.domain.FundTotal(year, ins.name, SUM(credit_guarantee_amount)) " +
            "FROM FundStatus fs LEFT JOIN Institute ins GROUP BY year, fs.institute")*/
    @Query(value = "SELECT YEAR AS Year, IST.NAME AS Institute_name, SUM(CREDIT_GUARANTEE_AMOUNT) AS Total_amount " +
            "FROM FUND_STATUS FS, INSTITUTE IST " +
            "WHERE FS.INSTITUTE_CODE = IST.INSTITUTE_CODE "+
            "GROUP BY FS.YEAR, IST.INSTITUTE_CODE", nativeQuery = true
    )
    List<Object[]> findTotalAmountGroupByYear();
}
