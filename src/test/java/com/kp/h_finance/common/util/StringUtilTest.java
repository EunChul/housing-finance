package com.kp.h_finance.common.util;

import com.kp.h_finance.common.predicate.InstituteNamePredicate;
import org.junit.Assert;
import org.junit.Test;

public class StringUtilTest {

    @Test
    public void isNumber() {
        String allNumber = "17389738309";
        String partOfNumber = "string8383";

        Assert.assertTrue(StringUtil.isNumber(allNumber));
        Assert.assertFalse(StringUtil.isNumber(partOfNumber));
    }

    @Test
    public void filtering() {
        String s1 = "농협은행/수협은행(억원)";
        String s2 = "주택도시기금1)(억원)";

        Assert.assertEquals(StringUtil.filtering(s1, new InstituteNamePredicate()), "농협은행/수협은행억원");
        Assert.assertEquals(StringUtil.filtering(s2, new InstituteNamePredicate()), "주택도시기금억원");
    }


    @Test
    public void getInstituteName() {
        String s1 = "농협은행/수협은행(억원)";
        String s2 = "주택도시기금1)(억원)";

        Assert.assertEquals(StringUtil.getInstituteName(s1), "농협은행/수협은행");
        Assert.assertEquals(StringUtil.getInstituteName(s2), "주택도시기금");
    }
}