package com.kp.h_finance.api.service;

import com.kp.h_finance.api.domain.FundTotalByBank;
import com.kp.h_finance.api.domain.FundTotalByYear;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.File;
import java.io.FileInputStream;
import java.util.List;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class FundStatusServiceTest {

    @Autowired
    private CSVFileService csvFileService;

    @Autowired
    private FundStatusService fundStatusService;

    @Before
    public void initData() {
        try {
            File file = new ClassPathResource("static/csv/testData.csv").getFile();
            MultipartFile multipartFile = new MockMultipartFile(file.toPath().toString(),
                    file.getName(), "application/csv", new FileInputStream(file));
            csvFileService.upload(multipartFile);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Test
    public void getAllInstitute() {
        Assert.assertNotNull(fundStatusService.getAllInstitute());
    }

    @Test
    public void getFundStatusTotal() {
        List<FundTotalByYear> fundTotalByYears = fundStatusService.getFundStatusTotal();
        Assert.assertEquals(fundTotalByYears.size(), 13);
        for (FundTotalByYear fundTotalByYear : fundTotalByYears) {
            Assert.assertEquals(fundTotalByYear.getDetail_amount().keySet().size(), 9);
        }
    }

    @Test
    public void getBiggestAmountBank() {
        Assert.assertNotNull(fundStatusService.getBiggestAmountBank().get("bank"));
        Assert.assertEquals("주택도시기금", fundStatusService.getBiggestAmountBank().get("bank"));
    }

    @Test
    public void getKoreaExchangeBankBigAndSmallFund() {
        Assert.assertNotNull(fundStatusService.getKoreaExchangeBankBigAndSmallFund().get("support_amount"));
        List<FundTotalByBank> result = (List<FundTotalByBank>) fundStatusService.getKoreaExchangeBankBigAndSmallFund().get("support_amount");
        Assert.assertEquals(result.size(), 2);
        Assert.assertEquals(result.get(0).getYear(), 2017);
        Assert.assertEquals(result.get(1).getYear(), 2015);
    }
}