package com.kp.h_finance.api.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FundTotalByBank {
    private int year;

    private String institute_name;
    private long total_amount;
}
