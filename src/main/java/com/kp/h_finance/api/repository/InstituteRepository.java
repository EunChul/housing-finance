package com.kp.h_finance.api.repository;

import com.kp.h_finance.api.domain.Institute;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InstituteRepository extends JpaRepository<Institute, Long> {

    Institute findByName(String name);
}
