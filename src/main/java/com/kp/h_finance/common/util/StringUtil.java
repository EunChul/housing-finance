package com.kp.h_finance.common.util;

import com.kp.h_finance.common.predicate.InstituteNamePredicate;
import com.kp.h_finance.common.predicate.NumberCheckPredicate;
import org.springframework.util.StringUtils;

import java.util.function.Predicate;
import java.util.stream.Collectors;

public class StringUtil {

    public static boolean isNumber(String s) {
        if (StringUtils.isEmpty(s)) {
            return false;
        }

        return new NumberCheckPredicate().test(s);
    }

    public static String filtering(String s, Predicate filter) {
        return s.codePoints().mapToObj(c -> String.valueOf((char) c))
                .filter(filter).collect(Collectors.joining()).toString();
    }

    public static String getInstituteName(String s) {
        return filtering(s, new InstituteNamePredicate())
                .replaceAll("억원", "");
    }
}
